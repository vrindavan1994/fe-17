const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.getElementById('root');
const ul = document.createElement('ul');
root.prepend(ul);

function isError(item, key) {
    throw new Error(`there is no such info about : ${key} - in this book : ${item.name}`)
}

books.forEach(item => {
    const {author, name, price} = item;
    try {
        if (!name) {
            isError(item, 'Name')
        } else if (!author) {
            isError(item, 'Author')
        } else if (!price) {
            isError(item, 'Price')
        }else {
            ul.insertAdjacentHTML('afterbegin', `<li>Author : ${author}<br> Name : ${name}<br> Price : ${price}</li>`)
        }
    } catch (e) {
        console.error(e)
    }
})

