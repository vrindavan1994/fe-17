const btn = document.createElement('button')
btn.className = 'btn'
btn.innerHTML = 'Вирохувати по IP'
document.body.append(btn)
btn.addEventListener('click',() => sendRequest())

async function sendRequest () {
    try {
        const getIp = await fetch("https://api.ipify.org/?format=json");
        const ip = await getIp.json();
        const ipInfo = await fetch(`http://ip-api.com/json/${ip.ip}?fields=continent,country,regionName,city,district`);
        const result = await ipInfo.json();

        if (result.district === ''){
             result.district = 'there is no information about this option'
        }
        let innerHTML = `
        <p>Continent : ${result.continent}</p>
        <p>Country : ${result.country}</p>
        <p>Region : ${result.regionName}</p>
        <p>City : ${result.city}</p>
        <p>District : ${result.district}</p>
        `

        const container = document.createElement('div')
        container.innerHTML = innerHTML
        document.body.append(container)


    } catch (e) {
        console.error(e)
    }
}












