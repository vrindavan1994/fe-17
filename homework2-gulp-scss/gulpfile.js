const gulp = require('gulp'),
    cleanCSS = require('gulp-clean-css'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create(),
    minifyjs = require('gulp-js-minify')
    // clean = require('gulp-clean')

const paths = {
    src: {
        styles: 'src/scss**/*.scss',
        script: 'src/script**/*.js',
        img:'src/img/**/*'
    },
    dist: {
        styles: 'dist/css/',
        script: 'dist/js/',
        img:'dist/img'
    }
}

const buildJS = () => (
    gulp.src(paths.src.script)
        .pipe(concat('script.min.js'))
        .pipe(minifyjs())
        .pipe(gulp.dest(paths.dist.script))
);
const buildSCSS = () => (
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest(paths.dist.styles))
);
const buildImg = () =>(
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
);
// const cleanBuild = () =>(
//     gulp.src(paths.build.html, {allowEmpty: true})
//         .pipe(clean())
// );
const runDev = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(paths.src.styles, buildSCSS).on('change', browserSync.reload);
    gulp.watch(paths.src.script, buildJS).on('change', browserSync.reload)
};

/*TASKS*/

gulp.task('buildJS', buildJS);
gulp.task('buildSCSS', buildSCSS);
gulp.task('buildImg', buildImg);
// gulp.task('cleanBuild', cleanBuild);
gulp.task('build', gulp.series(
    buildSCSS,
    buildJS,
    buildImg,
    // cleanBuild
));
gulp.task('dev', gulp.series(
    buildSCSS,
    buildJS,
    runDev
));