
const fetchPromise = fetch("https://swapi.dev/api/films/");
let mainContent = '',
    main = document.querySelector('.row');
fetchPromise.then(response => {
    return response.json();
}).then(data => {
    const result = Array.from(data.results);
    result.forEach(film => {
        mainContent += `
        <h3>Episode: ${film.episode_id}</h3>
  	    <h2>${film.title}</h2>
  	    <span>Characters: ${film.characters}</span>
  	    <h4>Epilogue: ${film.opening_crawl}</h4>    
`;
    });
    console.log(data);
    main.innerHTML = mainContent;
})



