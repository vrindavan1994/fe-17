class Employee {
    constructor(name,age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get getSalary (){
        return this.salary;
    }
    set getSalary(value){
        this.salary = value.split(' ')
    }
    get fullName (){
        return this.name;
    }
    set fullName(value){
        this.name = value.split(' ')
    }
    get getAge (){
        return this.age;
    }
    set getAge(value){
        this.age = value.split(' ')
    }
}
class Programmer extends Employee{
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this.lang = lang;
    }
    get getSalary (){
        return this.salary*3;
    }
}
const programmer = [
    new Programmer('John','26',100,'eng'),
    new Programmer('Mathew','45',200,'fr'),
    new Programmer('Michael','32',300,'ukr')
];
